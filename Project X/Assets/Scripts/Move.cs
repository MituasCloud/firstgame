﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour {
    Renderer Sphere;
    public float speed = 5.0f;
    public Material[] colors;
    int Rnd;

    void Start()
    {
        Sphere = GetComponent<Renderer>();
        Rnd = Random.Range(0, colors.Length);
        Sphere.material.color = colors[Rnd].color;
            

    }
    void Update () {
        transform.Translate(new Vector3(speed, 0.0f, 0.0f) * Time.deltaTime);
        if (transform.position.x >= 4)
        {
            Destroy(gameObject);
            HPManager.HP -= 1;
        }
            
	}
}
