﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Analysis : MonoBehaviour {

    Text text;
	void Start ()
    {
        text = GetComponent<Text>();
        if (Score.score >= 0 && Score.score <= 100) text.text = "Что-то слабовато!";
        else if(Score.score > 100 && Score.score <= 1000) text.text = "Неплохо! Для новичка :)";
        else if (Score.score > 1000 && Score.score <= 5000) text.text = "Ты был на высоте!";
        else if (Score.score > 5000 && Score.score <= 10000) text.text = "Слишком хорош для этого мира!";
        else if (Score.score > 10000 && Score.score <= 100000) text.text = "Эта игра создана для твоих пальцев!";
        else if (Score.score > 100000 && Score.score <= 1000000) text.text = "Кто этот парень!?";
        else if (Score.score > 1000000 && Score.score <= 10000000) text.text = "Господи, я знал что ты существуешь!";
        else if (Score.score > 10000000) text.text = "Я не знаю кто ты, но я готов склониться перед тобой!";

    }
	
}
