﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Example : MonoBehaviour {
    Renderer ren;
    GameObject go;
    public Material[] colors;
    int Cnt = 0;
    void Update () {
        Touch[] touches = Input.touches;
        if (Input.touchCount > 0)
        {
            for (int i = 0; i < touches.Length; i++)
            {
                if (Input.GetTouch(i).phase == TouchPhase.Began)
                {
                        Ray ray = Camera.main.ScreenPointToRay(touches[i].position);
                        RaycastHit hit = new RaycastHit();
                        if (Physics.Raycast(ray, out hit))
                        {
                            go = hit.transform.gameObject;
                            ren = go.GetComponent<Renderer>();
                            if (Cnt > 2) Cnt = 0;
                            ren.material.color = colors[Cnt].color;
                            Cnt++;
                        }
                   
                }
            }
        }
    }
}
