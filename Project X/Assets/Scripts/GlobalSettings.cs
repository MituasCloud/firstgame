﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GlobalSettings : MonoBehaviour {

    public static float musiclvl = 1;
    public static float soundslvl = 1;
    public static float difficultylvl = 0.8f;
    public Dropdown diff;

    public void Musiclvl(float lvl)
    {
        musiclvl = lvl;
    }

    public void Soundslvl(float lvl)
    {
        soundslvl = lvl;
    }

    public void Changelvl()
    {
        switch(diff.value)
        {
            case 0: difficultylvl = 0.8f; break;
            case 1: difficultylvl = 0.7f; break;
            case 2: difficultylvl = 0.6f; break;
            default: difficultylvl = 0.6f; break;
        }
    }

}
