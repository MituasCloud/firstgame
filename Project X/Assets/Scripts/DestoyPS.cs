﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestoyPS : MonoBehaviour {

    private ParticleSystem ps;
    AudioSource source;
    public AudioClip clip;

    void Start()
    {
        ps = gameObject.GetComponent<ParticleSystem>();
        source = GetComponent<AudioSource>();
        source.volume = GlobalSettings.soundslvl;
        source.clip = clip;
        source.Play();
    }
    void Update () {
        if (!ps.IsAlive())
            Destroy(gameObject);
	}
}
