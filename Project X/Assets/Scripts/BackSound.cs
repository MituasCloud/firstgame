﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class BackSound : MonoBehaviour {

    AudioSource source;
    public AudioClip[] backsounds;
    int i = 0;
	void Start () {
        source = GetComponent<AudioSource>();
        source.volume = GlobalSettings.musiclvl;
    }
	
	void FixedUpdate () {
        if (i > 9) i = 0;
		if(!source.isPlaying)
        {
            source.clip = backsounds[i];
            source.Play();
            i++;
        }
	}
}
