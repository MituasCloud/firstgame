﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Citation : MonoBehaviour {

    public string[] citation;
    Text text;

    void Start () {
        text = GetComponent<Text>();
        text.text = citation[Random.Range(0, citation.Length)];
	}
}
