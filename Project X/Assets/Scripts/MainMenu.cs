﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {

    public Text text;
    public GameObject ReadMe;
    static bool readflag = true;
    public void Start()
    {
        text.text = PlayerPrefs.GetInt("Best").ToString();
        if(readflag==true) ReadMe.SetActive(true);

    }
    public void StartGame()
    {
        SceneManager.LoadScene(1);
    }

    public void Settings()
    {
        SceneManager.LoadScene(3);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void HideReadMe()
    {
        ReadMe.SetActive(false);
        readflag = false;
    }
}
