﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GOScore : MonoBehaviour {

    Text text;

    void Start () {
        text = GetComponent<Text>();
        text.text = "Score: " + Score.score;
        if(PlayerPrefs.GetInt("Best") < Score.score)
        {
            PlayerPrefs.SetInt("Best", Score.score);
        }
    }

}
