﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Resolution : MonoBehaviour {
    public float ratio = 10f / 16f;
	// Use this for initialization
	void Start () {
        Camera cam = GetComponent<Camera>();
        cam.aspect = ratio;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
