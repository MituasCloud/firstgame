﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class ChangePlatformColor : MonoBehaviour
{
    AudioSource source;
    public AudioClip clip;
    Renderer ren;
    public Material[] colors;
    int i = 0;

    void Start()
    {
        source = GetComponent<AudioSource>();
        source.clip = clip;
        source.volume = GlobalSettings.soundslvl;
    }
    void OnMouseDown()
    {
        source.Play();
        ren = this.GetComponent<Renderer>();
        if (ren.material.color == Color.white) i = 0;
        if (i > 2) i = 0;
        ren.material.color = colors[i].color;
        i++;
    }
}