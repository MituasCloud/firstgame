﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HPManager : MonoBehaviour {

    public static int HP;
    Text text;

    void Awake()
    {
        text = GetComponent<Text>();
        HP = 3;
    }

    
    void Update () {
        text.text = "x" + HP;
    }
}
