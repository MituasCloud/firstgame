﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereGenerate : MonoBehaviour {
    public Vector3[] pos;
    public GameObject go;
    public float Time = 1f;
    public float TimeCoef = 0.05f;
    public float MinTime = 0.5f;


    int flag = 0;
    bool pause = true;
    int i = 0;

	void Start ()
    {
        MinTime = GlobalSettings.difficultylvl;
        StartCoroutine("CoroutineOfCreate");
    }

    IEnumerator CoroutineOfCreate()
    {
        while (true)
        {
            if (Time > MinTime) Time -= TimeCoef;

            if (flag == 0) i++;
            else if(flag == 1) i--;

            if (i == 0) flag = 0;
            else if (i == pos.Length - 1) flag = 1;

            if(pause)
            {
                pause = false;
                yield return new WaitForSeconds(1f);
            }

            CreateSphere(pos[i]);

            yield return new WaitForSeconds(Time);
        }
    }

    void CreateSphere(Vector3 pos)
    {
            Instantiate(go, pos, Quaternion.identity);
    }
}
