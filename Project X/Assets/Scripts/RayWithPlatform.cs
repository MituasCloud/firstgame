﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RayWithPlatform : MonoBehaviour {

    public GameObject PS;
    RaycastHit hit;
    Renderer RayRen;
    Renderer ThisRen;

    private void Start()
    {
        ThisRen = GetComponent<Renderer>();
    }
    void Update ()
    {
		if(Physics.Raycast(transform.position, -transform.up, out hit, 4f))
        {
            RayRen = hit.transform.gameObject.GetComponent<Renderer>();
            if (ThisRen.material.color == RayRen.material.color)
            {
                Destroy(gameObject);
                Instantiate(PS, transform.position, Quaternion.Euler(90,0,0));
                RayRen.material.color = Color.white;
                Score.score += 10;
            }

        }
	}
}
